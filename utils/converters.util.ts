export const moneyToNumber = (value: string) => {
  const formatedValue = value.replace(/[^0-9,]/g, '').replace(',', '.')
  return Number(formatedValue)
}
