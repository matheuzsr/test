/**
 * @description Remove todos os caracteres especiais de uma string
 * @param {string} value - String com caracteres especiais
 * @returns {string} - A string sem os caracters especiais
 */
export const removeSpecialsCharacters = (value = '') => {
  return value?.replace(/\W/g, '') || ''
}

/**
 * @description Valida se o CPF é valida ou não
 * @param {string}value - O valor digitado pelo usuário no input
 * @returns {string|boolean|string} Retorna **true** se o CPF for valido, do contrario retorna a mensagem de erro
 */
export const cpfValidation = (value: string) => {
  if (!value) { return false }
  value = removeSpecialsCharacters(value)
  if (value === '') { return false }

  if (
    value.length !== 11 ||
    value === '00000000000' ||
    value === '11111111111' ||
    value === '22222222222' ||
    value === '33333333333' ||
    value === '44444444444' ||
    value === '55555555555' ||
    value === '66666666666' ||
    value === '77777777777' ||
    value === '88888888888' ||
    value === '99999999999'
  ) { return 'Esse CPF não é válido' }

  let add = 0
  for (let i = 0; i < 9; i++) { add += parseInt(value.charAt(i)) * (10 - i) }
  let rev = 11 - (add % 11)
  if (rev === 10 || rev === 11) { rev = 0 }
  if (rev !== parseInt(value.charAt(9))) { return 'Esse CPF não é válido' }

  add = 0
  for (let i = 0; i < 10; i++) { add += parseInt(value.charAt(i)) * (11 - i) }
  rev = 11 - (add % 11)
  if (rev === 10 || rev === 11) { rev = 0 }
  return rev === parseInt(value.charAt(10)) || 'Esse CPF não é válido'
}

export const requiredRules = (value: string) => {
  return !!value || 'Este campo é obrigatório'
}
