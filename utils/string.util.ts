export const clearNumber = (str: string) => str.replace(/\D/g, '')

export const captalizeNewCharFromString = (str: string) => {
  const result = str.split('').map((char, index) => {
    if (index === 0) {
      char = char.toUpperCase()
    }

    return char
  })

  return result.join('')
}
