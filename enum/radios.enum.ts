// Construi este aquivo de enum para facilitar as alterações caso necessário,
// o ideal seriam estes dados virem da api, porém para todo caso,
// é extremamente fácil alterar, estando assim

export const radiosSpeciesPet = [
  {
    code: 'dog',
    text: 'Cachorro'
  },
  {
    code: 'cat',
    text: 'Gato'
  },
  {
    code: 'radio-other',
    text: 'Outros'
  }
]

export const breedCat = [
  { code: 'abissinio', text: 'Abissínio' },
  { code: 'angora', text: 'Angorá' },
  { code: 'ashera', text: 'Ashera' },
  { code: 'balines', text: 'Balinês' },
  { code: 'bengal', text: 'Bengal' },
  { code: 'radio-other', text: 'Outros' }
]

export const breedDog = [
  { code: 'shih-tzu', text: 'Shih tzu' },
  { code: 'yorkshire', text: 'Yorkshire' },
  { code: 'poodle', text: 'Poodle' },
  { code: 'phasa-apso', text: 'Lhasa apso' },
  { code: 'buldogue-frances', text: 'Buldogue francês' },
  { code: 'radio-other', text: 'Outros' }

]
