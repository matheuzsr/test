
import apiCountries from './apiCountries.service'

export interface Address {
cep: string
city: string
neighborhood: string
state: string
street: string
}

export const getCountriesFromRegion: any = async (region: string) => {
  try {
    const response = await apiCountries.get(`region/${region}`)
    return response
  } catch (err) {
    console.log('Erro ao buscar cep')
  }
}

export const getCountriefromPartName: any = async (name: string) => {
  try {
    const response = await apiCountries.get(`name/${name}`)
    return response
  } catch (err) {
    console.log('Erro ao buscar cep')
  }
}

export const getCountriesByLanguage: any = async (langCode: string) => {
  try {
    const response = await apiCountries.get(`lang/${langCode}`)
    return response
  } catch (err) {
    console.log('Erro ao buscar cep')
  }
}
