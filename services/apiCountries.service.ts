import axios from 'axios'

const apiCountries = axios.create({
  baseURL: process.env.VUE_APP_API_COUNTRIES
})

export default apiCountries
