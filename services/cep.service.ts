
import apiCep from '~/services/apiCep.service'

import { clearNumber } from '~/utils/string.util'
export interface Address {
cep: string
city: string
neighborhood: string
state: string
street: string
}

export const getAddress: any = async (cepUser: string) => {
  cepUser = clearNumber(cepUser)
  try {
    const response = await apiCep.get(`https://brasilapi.com.br/api/cep/v1/${cepUser}`)

    const { cep, city, neighborhood, state, street } = response.data

    return {
      cep,
      city,
      neighborhood,
      state,
      street
    }
  } catch (err) {
    console.log('Erro ao buscar cep')
  }
}
