import axios from 'axios'

const apiCep = axios.create({
  baseURL: process.env.VUE_APP_API_CEP
})

export default apiCep
