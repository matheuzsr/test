# Informações do projeto 
1) Acessando localhost:3000/
    No formulário eu alterei a ordem para ficar mais coerentes e melhor organizados

2) Acessando localhost:3000/countries
  - É exibido o pais, ao clicar abre uma aba com o Google Maps centralizado no país em questão

3) Ainda na tela localhost:3000/countries. Ao clicar em pesquisar país e inserir na busca o inicio do nome do país e ele inicia a procura.

4) Ainda nessa modal caso se clique em um dos idiomas do país, a aplicação deve listar os nomes de todos os países que possuem o idioma em comum.

5) Acessando localhost:3000/regions-information
    Há  um gráfico mostrando a área e população de cada país de uma região geográfica

## Build Setup

```bash
# install dependencies
$ yarn install

# serve with hot reload at localhost:3000
$ yarn dev

# build for production and launch server
$ yarn build
$ yarn start

# generate static project
$ yarn generate
```

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).
